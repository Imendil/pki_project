param(
	[string]$request="request.inf",
	[string]$password="Password",
	[string]$publicResult="certificatePub.cer",
	[string]$privateResult="certificatePriv.pfx"
)
certreq -new $request result.txt
Remove-Item .\$request
certreq -policy -cert - result.txt policy.inf result2.txt
certreq -submit -config - result2.txt $publicResult
Remove-Item .\result.txt
Remove-Item .\result2.txt
certreq -accept $publicResult
$serno = certutil -dump .\$publicResult | findstr Number:
$serial = $serno.Substring($serno.IndexOf(":")+2)
certutil -exportPFX -p $password my $serial $privateResult