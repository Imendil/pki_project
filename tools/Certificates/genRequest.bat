@echo off
echo [Version] > %4
echo Signature="$Windows NT$" >> %4
echo [NewRequest] >> %4 
echo Subject = "E = %1, CN = %2 %3" >> %4 
echo Exportable = TRUE >> %4 
echo KeyLength = 2048 >> %4 
echo KeySpec = 1 >> %4 
echo KeyUsage = 0xA0 >> %4 
echo MachineKeySet = True >> %4 
echo ProviderName = "Microsoft RSA SChannel Cryptographic Provider" >> %4 
echo ProviderType = 12 >> %4 
echo RequestType = CMC >> %4 
echo [EnhancedKeyUsageExtension] >> %4 
echo OID=1.3.6.1.5.5.7.3.4 >> %4