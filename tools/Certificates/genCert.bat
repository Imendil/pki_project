@echo off
REM Użycie: genCert.bat Email Imie Nazwisko NazwaPlikuInfZRequestem Password NazwaPlikuZCertyfikatem NazwaPlikuZKluczem
REM Na przykład: genCert.bat example@example.com Janusz Typek request.inf 123 cert.cer cer.pfx
genRequest %1 %2 %3 %4 && powershell -command .\genCerts.ps1 -request \"%4\" -password \"%5\" -publicResult \"%6\" -privateResult \"%7\"