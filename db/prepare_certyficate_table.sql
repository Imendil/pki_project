ALTER TABLE `symfony`.`certificate` 
CHANGE COLUMN `create_date` `create_date` DATETIME NULL ,
CHANGE COLUMN `expiration_date` `expiration_date` DATETIME NULL ,
CHANGE COLUMN `certificate_file` `certificate_file` LONGBLOB NULL ,
CHANGE COLUMN `alias` `alias` VARCHAR(255) CHARACTER SET 'utf8' NULL ;
