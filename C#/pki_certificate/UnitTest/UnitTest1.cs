﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using pki_certificate;
using System.IO;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        const int EXIT_EXPECTED = 0;
        const int TEST_USER_ID = 1;
        const string TEST_FILE_NAME_CA = "test.cer";
        const string TEST_FILE_NAME_PFX = "test.pfx";
        const string TEST_PASSWORD = "test";        
         
        [TestMethod]
        public void TestGenerateCertificate()
        {
            var pki = new PKI();
            var user = new Database().SelectUser(TEST_USER_ID);

            Assert.IsNotNull(user, "User not exist in database");

            var result = pki.GenerateCertificate(user);

            Assert.AreEqual<int>(EXIT_EXPECTED, result, "Error during generating certificate");

            var cert = new Database().SelectCertificate(pki.certificate.SerialNumber);

            Assert.IsNotNull(cert, "Certificate not exist in database");                        
            
        }

        [TestMethod]
        public void TestGetCertificate()
        {
            var pki = new PKI();
            var user = new Database().SelectUser(TEST_USER_ID);
            pki.GenerateCertificate(user);

            pki.GetCert(pki.certificate.SerialNumber, TEST_FILE_NAME_CA);            

            Assert.IsFalse(!File.Exists(pki.CA_FILE), "File not exist");
        }

        [TestMethod]
        public void TestGetPrivateKey()
        {
            var pki = new PKI();
            var user = new Database().SelectUser(TEST_USER_ID);
            pki.GenerateCertificate(user);

            pki.GetPrivateKey(pki.certificate.SerialNumber, TEST_PASSWORD ,TEST_FILE_NAME_PFX);           

            Assert.IsFalse(!File.Exists(pki.PFX_FILE), "File not exist");
        }

        [TestMethod]
        public void TestValidate()
        {
            var pki = new PKI();
            var user = new Database().SelectUser(TEST_USER_ID);
            pki.GenerateCertificate(user);

            pki.GetCert(pki.certificate.SerialNumber, TEST_FILE_NAME_CA);            

            Assert.IsFalse(!File.Exists(pki.CA_FILE), "File not exist");
            Assert.AreEqual<int>(EXIT_EXPECTED, pki.Validate(pki.CA_FILE), "Validation failed");
        }

        [TestMethod]
        public void TestRevoke()
        {
            var pki = new PKI();
            var user = new Database().SelectUser(TEST_USER_ID);
            pki.GenerateCertificate(user);

            pki.GetCert(pki.certificate.SerialNumber, TEST_FILE_NAME_CA);            

            Assert.IsFalse(!File.Exists(pki.CA_FILE), "File not exist");
            Assert.AreEqual<int>(EXIT_EXPECTED, pki.Revoke(pki.certificate.SerialNumber, "Superseded"), "Revoke failed");
        }

    }
}
