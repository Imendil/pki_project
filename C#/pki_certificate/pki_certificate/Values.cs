﻿using Org.BouncyCastle.Asn1.X509;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pki_certificate
{
    public static class Values
    {
        public static string Path = AppDomain.CurrentDomain.BaseDirectory;
        public static string CN = "ZUT_PROJEKT";
        public static Dictionary<string, string> TypeOfCert = new Dictionary<string, string>
        {
            { "CodeSigning", "1.3.6.1.5.5.7.3.3" },
            { "EmailProtection" ,"1.3.6.1.5.5.7.3.4"},
            { "IpsecEndSystem" ,"1.3.6.1.5.5.7.3.5"},
            { "IpsecTunnel","1.3.6.1.5.5.7.3.6" },
            { "OscpSigning","1.3.6.1.5.5.7.3.9" },
            { "ServerAuth","1.3.6.1.5.5.7.3.1" },            
            { "TimeStamping","1.3.6.1.5.5.7.3.8"},
        };

        public static Dictionary<string, KeyPurposeID> TypeOfSelfSignedCert = new Dictionary<string, KeyPurposeID>
        {
            { "CodeSigning", KeyPurposeID.IdKPCodeSigning},
            { "EmailProtection" ,KeyPurposeID.IdKPEmailProtection},
            { "IpsecEndSystem" ,KeyPurposeID.IdKPIpsecEndSystem},
            { "IpsecTunnel",KeyPurposeID.IdKPIpsecTunnel},
            { "OscpSigning",KeyPurposeID.IdKPOcspSigning },
            { "ServerAuth",KeyPurposeID.IdKPServerAuth},
            { "TimeStamping",KeyPurposeID.IdKPTimeStamping},
        };

        public static Dictionary<string, int> TypeOfRevoke = new Dictionary<string, int>
        {
            {" Unspecified", 0 },
            { "Key Compromise", 1  },
            { "CA Compromise", 2 },
            { "Affiliation Changed", 3 },
            { "Superseded", 4 },
            {"Cessation of Operation", 5 }
        };

        public static string SelfSigned = "self";
        public static string CASigned = "ca";

         public static string RequestFile = AppDomain.CurrentDomain.BaseDirectory + "request.inf";
         public static string ResultFile = AppDomain.CurrentDomain.BaseDirectory + "result.txt";
         public static string CAFile = AppDomain.CurrentDomain.BaseDirectory + "public_ca.cer";
         public static string PFXFile = AppDomain.CurrentDomain.BaseDirectory + "private_key.pfx";
         public static string PoliceEmail = AppDomain.CurrentDomain.BaseDirectory + "police.inf";
         public static string PoliceResult = AppDomain.CurrentDomain.BaseDirectory + "result2.txt";
    }
}
