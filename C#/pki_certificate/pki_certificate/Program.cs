﻿using System;

namespace pki_certificate
{
    class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static void Main(string[] args)
        {
            log.Info("Starting program, args:  " + args.Length);           
            
            if (args.Length > 0)
            {
                try
                {                    
                    var cmd = args[0];
                    int exit = 0;
                    User user;
                    switch (cmd)
                    {
                        case "generateCertificate":
                            user = new Database().SelectUser(int.Parse(args[1]));
                            exit = new PKI().GenerateCertificate(user);
                            break;
                        case "getCert":
                            exit = new PKI().GetCert(args[1], args[2]);
                            break;
                        case "getPrivateKey":
                            exit = new PKI().GetPrivateKey(args[1], args[2], args[3]);
                            break;
                        case "validate":
                            exit = new PKI().Validate(args[1]);
                            break;
                        case "revoke":
                            exit = new PKI().Revoke(args[1], args[2]);
                            break;
                        default:
                            exit = -1;
                            log.Error("Not recognized command");
                            break;
                            
                    }
                    
                    Environment.ExitCode = exit;
                }
                catch(Exception e)
                {
                    log.Error("Program cannot be run: ", e);
                    Environment.ExitCode = -1;
                }              
            }
            else
            {
                log.Info("Incorrect number of args.");
            }
        }
    }
    
}
