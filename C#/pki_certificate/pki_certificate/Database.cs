﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pki_certificate
{
    public class Database
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private MySqlConnection connection;
        private string server;
        private string database;
        private string uid;
        private string password;

        //Constructor
        public Database()
        {
            Initialize();
        }

        //Initialize values
        private void Initialize()
        {
            server = "localhost";
            database = "symfony";
            uid = "root";
            password = "";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

            connection = new MySqlConnection(connectionString);
            log.Info("Connection to db works");
        }

        public User SelectUser(int id)
        {
            log.Info("Select user form db id: " + id.ToString());
            var query = "SELECT * from user where id=" + id.ToString() ;
            User user = new User() ;

            if (this.OpenConnection())
            {               
                MySqlCommand cmd = new MySqlCommand(query, connection);              
                MySqlDataReader dataReader = cmd.ExecuteReader();
                                               
                while (dataReader.Read())
                {
                    user.Id = int.Parse(dataReader["id"].ToString());
                    user.Name = dataReader["name"].ToString();
                    user.Surname = dataReader["surname"].ToString();
                    user.UserName = dataReader["username"].ToString();
                    user.Company = dataReader["companyName"].ToString();
                    user.Email = dataReader["email"].ToString();
                }

                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();                
            }            

            return user;
        }

        public Certificate SelectCertificate(string serialNumber)
        {
            log.Info("Select certificate from db serial: " + serialNumber);
            var query = string.Format("SELECT * from certificate where serial_number='{0}'",serialNumber);
            Certificate cert= new Certificate();

            if (this.OpenConnection())
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {                    
                    cert.UserId = int.Parse(dataReader["user_id"].ToString());
                    cert.ExpiredDate = (DateTime)dataReader["expiration_date"];
                    cert.CreateDate = (DateTime)dataReader["create_date"];
                    cert.SerialNumber = dataReader["serial_number"].ToString();
                    cert.Revoked = (bool)dataReader["revoked"];                          
                }

                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }

            return cert;
        }

        public void InsertCertificate(Certificate cert)
        {
            log.Info("Insert certificate in db");
            var query = @"INSERT INTO certificate
                (user_id,
                create_date,
                expiration_date,                
                serial_number,                
                type,               
                revoked)
                VALUES
                (?user_id,
                ?create_date,
                ?expiration_date,                
                ?serial_number,                
                ?type,                
                ?revoked)";

            if(this.OpenConnection())
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                cmd.Parameters.AddWithValue("user_id", cert.UserId);
                cmd.Parameters.AddWithValue("create_date", cert.CreateDate);
                cmd.Parameters.AddWithValue("expiration_date", cert.ExpiredDate);                
                cmd.Parameters.AddWithValue("serial_number", cert.SerialNumber);               
                cmd.Parameters.AddWithValue("type", cert.Type);
                cmd.Parameters.AddWithValue("revoked", cert.Revoked);                
                cmd.ExecuteNonQuery();

                this.CloseConnection();
            }
        }

        //open connection to database
        private bool OpenConnection()
        {
            try
            {
                log.Info("Open connection");
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                log.Error("Connection error!", ex);
                return false;
            }
        }

        //Close connection
        private bool CloseConnection()
        {
            try
            {
                log.Info("Close connection");
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                log.Error("Error: ", ex);
                return false;
            }
        }
    }
}
