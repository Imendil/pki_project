﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pki_certificate
{
    public class Certificate
    {
        public int UserId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ExpiredDate { get; set; }
        public string SerialNumber { get; set; }
        public string Type { get; set; }
        public bool Revoked { get; set; }                  
    }
}
