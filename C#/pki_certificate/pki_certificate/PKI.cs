﻿using CERTCLIENTLib;
using CERTENROLLLib;
using Org.BouncyCastle.Crypto.Prng;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.Security;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

using System.Text;
using System.Threading.Tasks;

namespace pki_certificate
{
    public class PKI
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const int CC_DEFAULTCONFIG = 0;
        private const int CC_UIPICKCONFIG = 0x1;

        ProcessStartInfo processInfo;
        CCertConfig objCertConfig;
        string strCAConfig;

        //For test
        public Certificate certificate;
        public string CA_FILE;
        public string PFX_FILE;

        public PKI()
        {
            processInfo  = new ProcessStartInfo();
            processInfo.WindowStyle = ProcessWindowStyle.Hidden;

            objCertConfig = new CCertConfigClass();
            strCAConfig = objCertConfig.GetConfig(CC_DEFAULTCONFIG);
        }
               
        public int GenerateCertificate(User user)
        {
            log.Info("Starting generate CA certificate...");            

            try
            {
               
                log.Info("CA config: " + strCAConfig);              

                var req = this.Request(string.Format("E = {0};CN = {1} {2}",user.Email, user.Name, user.Surname), "1.3.6.1.5.5.7.3.4");

                log.Info("Creating request.inf file - " + Values.RequestFile);               
                var file = File.CreateText(Values.RequestFile);
                file.Write(req);
                file.Close();

                var polcieReq = this.PoliceRequest("1.3.6.1.5.5.7.3.4");
                log.Info("Creating police.inf file - " + Values.PoliceEmail);
                var filep = File.CreateText(Values.PoliceEmail);
                filep.Write(polcieReq);
                filep.Close();

                
                processInfo.FileName = "certreq.exe";
                processInfo.WorkingDirectory = Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory);
                processInfo.UseShellExecute = false;                                

                processInfo.Arguments = string.Format("-new {0} {1}", Values.RequestFile, Values.ResultFile);
                log.Info("Cmd certreq.exe " + processInfo.Arguments);
                Process.Start(processInfo).WaitForExit();

                processInfo.Arguments = string.Format("-policy -cert - {0} {1} {2}", Values.ResultFile, Values.PoliceEmail, Values.PoliceResult);
                log.Info("Cmd certreq.exe " + processInfo.Arguments);
                Process.Start(processInfo).WaitForExit();

                processInfo.Arguments = string.Format("-submit -config {0} {1} {2}", strCAConfig, Values.PoliceResult, Values.CAFile);
                log.Info("Cmd certreq.exe " + processInfo.Arguments);
                Process.Start(processInfo).WaitForExit();
                
                processInfo.Arguments = "-accept " + Values.CAFile;
                log.Info("Cmd certreq.exe " + processInfo.Arguments);
                Process.Start(processInfo).WaitForExit();

                var cert = new X509Certificate2(Values.CAFile);

                certificate = new Certificate
                {
                    CreateDate = cert.NotBefore,
                    ExpiredDate = cert.NotAfter,
                    SerialNumber = cert.SerialNumber,
                    Revoked = false,
                    UserId = user.Id,
                    Type = "EmailProtection"
                };

                log.Info("Insert cert in db");
                new Database().InsertCertificate(certificate);

                log.Info("Delete files");
                File.Delete(Values.PoliceResult);
                File.Delete(Values.RequestFile);
                File.Delete(Values.ResultFile);
                File.Delete(Values.CAFile);
                File.Delete(Values.PoliceEmail);
            }
            catch (Exception ex)
            {
                log.Error("CA failed!", ex);
                return -1;
            }
            log.Info("CA generated successfully");
            return 0;
        }

        public int GetCert(string serial, string CaName)
        {
            var path = AppDomain.CurrentDomain.BaseDirectory + CaName;

            if (File.Exists(path))
            {
                File.Delete(path);
            }

            processInfo.FileName = "certreq.exe";            
            processInfo.Arguments = String.Format("-retrieve -config {0} {1} {2} ", strCAConfig,  serial ,path);
            log.Info("Cmd certreq.exe " + processInfo.Arguments);
            Process.Start(processInfo).WaitForExit();

            CA_FILE = path;
            return 1;     
        }

        public int GetPrivateKey(string serial, string password, string PfxName)
        {
            var path = AppDomain.CurrentDomain.BaseDirectory + PfxName;

            if(File.Exists(path))
            {
                File.Delete(path);
            }

            processInfo.FileName = "certutil.exe";
            processInfo.Arguments = String.Format("-exportPFX -p \"{0}\" my {1} {2}", password, serial as String, path);
            log.Info("Cmd certutil.exe " + processInfo.Arguments);
            processInfo.UseShellExecute = false;
            Process.Start(processInfo).WaitForExit();

            PFX_FILE = path;
            return 1;
        }

        public int Validate(string path)
        {
            try
            {
                log.Info("Starting validate...");
                log.Info("Loading certificate...");
                var c = new System.Security.Cryptography.X509Certificates.X509Certificate2();
                c.Import(path);

                CCertConfig objCertConfig = new CCertConfigClass();
                var cert = new CERTADMINLib.CCertAdminClass();
                var strCAConfig = objCertConfig.GetConfig(CC_DEFAULTCONFIG);
                log.Info("Validating result: " + cert.IsValidCertificate(strCAConfig, c.SerialNumber).ToString());
                return (cert.IsValidCertificate(strCAConfig, c.SerialNumber) == 3) ? 0 : -1;
            }
            catch(Exception ex)
            {
                log.Error("Error ", ex);
                return -1;
            }
            
        }

        public int Revoke(string serialNumber, string reason)
        {
            try
            {
                log.Info("Starting revoke...");
                CCertConfig objCertConfig = new CCertConfigClass();
                var cert = new CERTADMINLib.CCertAdminClass(); 
                var strCAConfig = objCertConfig.GetConfig(CC_DEFAULTCONFIG);

                cert.RevokeCertificate(strCAConfig, serialNumber, Values.TypeOfRevoke[reason], DateTime.Now.Subtract(new TimeSpan(1, 1, 1)));
                log.Info("Certificate was revoked");
            }
            catch(Exception ex)
            {
                log.Error("Error ", ex);
                return -1;
            }
            return 0;
        }

        private string Request(string subject, string type)
        {

            var request = new StringBuilder();
            request.Append(String.Format("[Version]\nSignature=\"$Windows NT$\"\n\n[NewRequest]\nSubject = \"{0}\"\nExportable = TRUE\nKeyLength = 2048\n", subject));
            request.Append("KeySpec = 1\nKeyUsage = 0xA0\nMachineKeySet = True\nProviderName = \"Microsoft RSA SChannel Cryptographic Provider\"\n");
            request.Append(String.Format("ProviderType = 12\nRequestType = CMC\n\n"));
            request.Append(String.Format("[EnhancedKeyUsageExtension]\nOID={0}\n",type));

            return request.ToString();
        }

        private string PoliceRequest(string type)
        {
            var request = new StringBuilder();
            request.Append("[Version]\nSignature= \"$Windows NT$\"\n\n[ApplicationPolicyStatementExtension]\n\nPolicies = AppEmailPolicy\n\n");
            request.Append(String.Format("[AppEmailPolicy]\nOID = {0} ; Secure Email\n", type));
            return request.ToString();
        }
    }
}
