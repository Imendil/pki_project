<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Certificate
 *
 * @ORM\Table(name="`certificate`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CertificateRepository")
 */
class Certificate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_date", type="datetime")
     */
    private $createDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expiration_date", type="datetime")
     */
    private $expirationDate;
    
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string")
     */
    private $type;
    
    /**
     * @var bool
     *
     * @ORM\Column(name="revoked", type="boolean")
     */
    private $revoked;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="serial_number", type="string")
     */
    private $serialNumber;
    
    /**
    * @var string
    *
    * @ORM\Column(name="revoke_reason", type="string")
    */
    private $revokeReason;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return Certificate
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set expirationDate
     *
     * @param \DateTime $expirationDate
     *
     * @return Certificate
     */
    public function setExpirationDate($expirationDate)
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }

    /**
     * Get expirationDate
     *
     * @return \DateTime
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    /**
     * Set revoked
     *
     * @param boolean $revoked
     *
     * @return Certificate
     */
    public function setRevoked($revoked)
    {
        $this->revoked = $revoked;

        return $this;
    }

    /**
     * Get revoked
     *
     * @return bool
     */
    public function getRevoked()
    {
        return $this->revoked;
    }

    /**
     * Set user
     *
     * @param User $userId
     *
     * @return Certificate
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
	
	/**
	 * Get type
	 *
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}
	
	/**
	 * Set type
	 *
	 * @param string $type
	 *
	 * @return Certificate
	 */
	public function setType($type) {
		$this->type = $type;
		return $this;
	}
	
	/**
	 * Get serialNumber
	 *
	 * @return string
	 */
	public function getSerialNumber() {
		return $this->serialNumber;
	}
	
	/**
	 * Set serialNumber
	 *
	 * @param string $type
	 *
	 * @return Certificate
	 */
	public function setSerialNumber($serialNumber) {
		$this->serialNumber = $serialNumber;
		return $this;
	}

	/**
	 * Set revokeReason
	 *
	 * @param string $revokeReason
	 *
	 * @return Certificate
	 */
	public function setRevokeReason($revokeReason) {
		$this->revokeReason = $revokeReason;
		return $this;
	}
	
	/**
	 * Get $revokeReason
	 *
	 * @return string
	 */
	public function getRevokeReason(){
		return $this->revokeReason;
	}
    
    
}

