<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="`user`")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string")
     */
    private $surname;
    
    /**
     * @var string
     *
     * @ORM\Column(name="companyName", type="string")
     */
    private $companyName;
    
    
    public function __construct()
    {
        parent::__construct();
        // your own logic
    }
    
    /**
     * Get id
     *
     * @return integer
     */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}
	
	/**
	 * Set name
	 *
	 * @param string $name
	 *
	 * @return User
	 */
	public function setName($name) {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * Get surname
	 *
	 * @return string
	 */
	public function getSurname() {
		return $this->surname;
	}
	
	/**
	 * Set surname
	 *
	 * @param string $surname
	 *
	 * @return User
	 */
	public function setSurname($surname) {
		$this->surname = $surname;
		return $this;
	}
	
	/**
	 * Get companyName
	 *
	 * @return string
	 */
	public function getCompanyName() {
		return $this->companyName;
	}
	
	/**
	 * Set companyName
	 *
	 * @param string $companyName
	 *
	 * @return User
	 */
	public function setCompanyName($companyName) {
		$this->companyName = $companyName;
		return $this;
	}
	
	
	
	
}