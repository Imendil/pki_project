<?php
namespace AppBundle\Controller;

use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;


class SecurityController extends BaseController
{
	public function loginAction(Request $request)
	{		
		$check_login = $this->checkAuth();
		if ($check_login){
			return $this->redirectToRoute('list');
		} else {
			$res = parent::loginAction($request);
			return $res;
		}		
	}
	
	private function checkAuth(){
		$securityContext = $this->container->get('security.authorization_checker');
		if (!$securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
			return false;
		} else {
			return true;
		}
	}

}