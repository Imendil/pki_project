<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity;

use FOS\UserBundle\Controller\RegistrationController as BaseController;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

define("PATH_EXE", "../bin/pki_certificate.exe");

class PkiController extends BaseController
{
	/**
	 * @Route("/login", name="login")
	 */
	public function loginAction(){
		return $this->redirectToRoute('homepage');
	}
	
	
    /**
     * @Route("/home", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
    	$securityContext = $this->container->get('security.authorization_checker');
    	if ($securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
    		return $this->redirectToRoute('list');
//     		return $this->render('pki/list.html.twig', array(
//     		//return $this->render('FOSUserBundle/views/Security/login.html.twig', array(
//             'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
//         	));
    	} else {
    		return $this->redirectToRoute('fos_user_security_login');
    	}        
    }
    

    /**
     * @Route("/profile", name="profile")
     */
    public function profileAction(){
    	$this->checkAuth();
    	return $this->render('pki/profile.html.twig', array('user' => $this->getUser()));
    }
    
    /**
     * @Route("/list", name="list")
     */
    public function listAction(){
    	$this->checkAuth();
    	$req = Request::createFromGlobals();
    	$repo = $this->getDoctrine()->getRepository('AppBundle:Certificate');
    	if($req->isMethod('POST')){
    		$by = $req->request->get('searchby');
    		$filter = array('user' => $this->getUser());
    		switch($by){
    			case 'Creation date':
    				$list = $repo->findByCreateDate($this->getUser(), $req->request->get('create_date'));
	    			break;
    			case 'Expiration date':
	    			$list = $repo->findByExpireDate($this->getUser(), $req->request->get('expiration_date'));
	    			break;
    			case 'Type':
	    			$filter['type'] = $req->request->get('type');
		     		$list = $repo->findBy($filter);
    				break;
    			case 'Status':
    				$list = $repo->findByStatus($this->getUser(), $req->request->get('status'));
    				break;
    		}
    	} else {
	    	$list = $this->getDoctrine()
		    	->getRepository('AppBundle:Certificate')
		    	->findByUser($this->getUser());
    	}
    	return $this->render('pki/list.html.twig', array('list' => $list));
    }
    
    /**
     * @Route("/generate", name="generate")
     */
    public function generateAction(){
    	$this->checkAuth();
    	$user = $this->getUser();
    	return $this->render('pki/generate.html.twig', array("user" => $user));
    }

    /**
     * @Route("/details/{id}", name="details")
     */
    public function detailsAction($id){
    	$this->checkAuth();
    	$cert = $this->getDoctrine()
	    	->getRepository('AppBundle:Certificate')
	    	->findOneById($id);
    	$user = $cert->getUser();
    	return $this->render('pki/details.html.twig', array("cert" => $cert, "user" => $user));
    }

    /**
     * @Route("/revoke", name="revoke")
     */
    public function revokeAction(){
    	$this->checkAuth();
		$id = Request::createFromGlobals()->request->get('id');
		$reason = Request::createFromGlobals()->request->get('reason');
    	$em = $this->getDoctrine()->getManager();
    	$cert = $em->getRepository('AppBundle:Certificate')->findOneById($id);
		$serial = $cert->getSerialNumber();
		$command =  "\"".PATH_EXE."\" revoke $serial \"$reason\"";

		
		exec($command, $output, $return);
		
		if($return == 0) {
			$cert->setRevoked(true);
			$cert->setRevokeReason($reason);
		    $em->flush();
		    $this->addFlash('notice', 'Certificate has been revoked');
		} else {
			$this->addFlash('notice', 'Certificate has not been revoked');
		}
		return $this->redirectToRoute('list');
    }
    
    /**
     * @Route("/download", name="download")
     */
    public function downloadAction(){
    	$this->checkAuth();
		$cert_type = Request::createFromGlobals()->request->get('cert');
		$id = Request::createFromGlobals()->request->get('id');
		$tmpfname = "../tmp/".uniqid("pki").".tmp";
		$cert = $this->getDoctrine()
			->getRepository('AppBundle:Certificate')
			->findOneById($id);
		if (!$cert) {
			throw $this->createNotFoundException("File not exist!");
		}
		$serial = $cert->getSerialNumber();
		
		if($cert_type == 'private'){
			$pass = Request::createFromGlobals()->request->get('pass');
			$command = "\"".PATH_EXE."\" getPrivateKey $serial $pass \"$tmpfname\"";
		} else {
			$command = "\"".PATH_EXE."\" getCert $serial \"$tmpfname\"";
		}
		
		exec($command, $output, $return);
		
		if($return == 1) {
	    	$extension = $cert_type == 'public' ? 'cer' : 'pfx';
	    	$handle = fopen($tmpfname, "r");
	    	$file = fread($handle, filesize($tmpfname));
	    	$response = new Response($file, 200, array(
	    			'Content-Type' => 'application/octet-stream',
	    			'Content-Length' => sizeof($file),
	    			'Content-Disposition' => 'attachment; filename="certyfikat.' . $extension . '"'
	    	));
			fclose($handle);unlink($tmpfname);
	    	return $response;
		} else {
		    $this->addFlash('notice', 'Internal error');
		    fclose($handle);unlink($tmpfname);
		    return $this->redirectToRoute('list');
		}
    }
    
    /**
     * @Route("/create", name="create")
     */
    public function createAction(){
    	$this->checkAuth();
    	$id = $this->getUser()->getId();
    	$req = Request::createFromGlobals();
    	$password = $req->request->get('pass');
    	$type = $req->request->get('type');
    	
    	$command =  "\"".PATH_EXE."\" generateCertificate $id";
    	
    	exec($command, $output, $return);
    	
    	if($return == 0)
    		$this->addFlash('notice', 'Certificate created');
    	else
    		$this->addFlash('notice', 'Error: certificate is not generated');
    	
    	return $this->redirectToRoute('list');
    }
    
    /**
     * @Route("/validate", name="validate")
     */
    public function validateAction(){
    	//$this->checkAuth();
    	return $this->render('pki/validate.html.twig');
    }
    
    /**
     * @Route("/dovalid", name="dovalid")
     */
    public function dovalidAction(){
    	//$this->checkAuth();

    	$file = $_FILES["certyficateFile"]["tmp_name"];
    	$command = "\"".PATH_EXE."\" validate \"$file\"";
    	
    	$fileType = pathinfo($_FILES["certyficateFile"]["name"],PATHINFO_EXTENSION);
    	
    	if($fileType == 'cer') {
	    	exec($command, $output, $result);
	    	if($result == 0){
	    		$this->addFlash('notice', 'Certificate is Valid');
	    	} else {
	    		$this->addFlash('notice', 'Certificate is invalid');
	    	}
    	} else if($fileType == 'pfx') {
	    	$this->addFlash('notice', 'We do not validate such certificates, because the owner password is required');
    	} else {
	    	$this->addFlash('notice', 'It is not Certificate');
    	}
    	return $this->redirectToRoute('validate');
    }
    
    /**
     * @Route("/changepass", name="changepass")
     */
    public function changePassAction(Request $request){
    	$this->checkAuth();
    	if(Request::createFromGlobals()->isMethod('POST')){
	    	$user = $this->getUser();
	    	$new_pass = Request::createFromGlobals()->request->get('new_pass');
	    	$plain_pass = Request::createFromGlobals()->request->get('plain_pass');
	
	    	$valid = true;
	    	if($new_pass !== $plain_pass){
	    		$valid = false;
	        	$this->addFlash('notice', 'Passwords do not match');
	    	} else {
	    		$user->setPassword($new_pass);
	    		$user->setPlainPassword($plain_pass);
	            $userManager = $this->get('fos_user.user_manager');
				$userManager->updateUser($user);
	
	            $this->addFlash('notice', 'Password has been changed');
	        }
    	}

        return $this->render('pki/changePassword.html.twig');
    }
    
    private function checkAuth(){
    	$securityContext = $this->container->get('security.authorization_checker');
    	if (!$securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
    		throw new AccessDeniedException('This user does not have access to this section.');
    	}
    }
}
