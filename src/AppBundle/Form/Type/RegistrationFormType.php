<?php 
namespace AppBundle\Form\Type;

use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class RegistrationFormType extends BaseType {
		
	public function buildForm(FormBuilderInterface $builder, array $options)
	{	
		parent::buildForm($builder, $options);
		
		$builder
		->add('surname', null, array(
				'label' => 'Surname', 
				'translation_domain' => 'FOSUserBundle',
				'required' => true
		))
		->add('name', null, array(
				'label' => 'Name', 
				'translation_domain' => 'FOSUserBundle',
				'required' => true
				
		))
		->add('companyName', null, array(
				'label' => 'Name of the organization', 
				'translation_domain' => 'FOSUserBundle',
				'required' => true
				
		));
	}
	
	public function getBlockPrefix()
	{
		return 'acme_user_registration';
	}
	
	public function getName()
	{
		return 'acme_user_registration';
	}
}